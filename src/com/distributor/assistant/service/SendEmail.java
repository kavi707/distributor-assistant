package com.distributor.assistant.service;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;

/**
 * Created by kavi on 9/30/13.
 */
public class SendEmail {

    public Intent sendEmailToGivenAddress () {

        String strFile = "";

        try {
            strFile = Environment.getExternalStorageDirectory()
                    .getAbsolutePath() + "/fauzias/latest_yummys";

            File file = new File(strFile);
            if (!file.exists())
                file.mkdirs();
            strFile = strFile + "/icon_1.png";

            Log.i(getClass().getSimpleName(), "send  task - start");
        }catch (Exception e) {

        }

        String to = "kavi707@gmail.com";
        String subject = "Test Email from Android";
        String message = "Test email message from android device";

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + strFile));

        emailIntent.setType("plain/text");

        return emailIntent;
    }
}
