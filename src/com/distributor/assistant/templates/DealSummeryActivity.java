package com.distributor.assistant.templates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.*;
import com.distributor.assistant.R;
import com.distributor.assistant.database.Customer;
import com.distributor.assistant.database.Deal;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;
import com.distributor.assistant.database.ProductDeal;

import java.util.List;

/**
 * Created by kavi on 10/3/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class DealSummeryActivity extends Activity {

    private TableLayout tableView;
    private TextView dealNameText;
    private TextView dealCustomerNameText;
    private TextView dealCustomerContactText;
    private TextView dealDateText;

    private EditText wholeDealDiscountEditText;
    private Button wholeDealDiscountAddButton;
    private TextView finalTotalTextView;
    private Button sendDealMailButton;

    private LinearLayout discountLayout;

    private long dealId;
    private int routePath = 0;
    private Deal getDeal;
    private Customer getCustomer;
    private List<ProductDeal> productDealsList;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deal_summery_layout);

        setUpView();
    }

    private void setUpView() {

        tableView = (TableLayout) findViewById(R.id.tableView);
        dealNameText = (TextView) findViewById(R.id.dealNameText);
        dealCustomerNameText = (TextView) findViewById(R.id.dealCustomerName);
        dealCustomerContactText = (TextView) findViewById(R.id.dealCustomerContact);
        dealDateText = (TextView) findViewById(R.id.dealDateText);

        wholeDealDiscountEditText = (EditText) findViewById(R.id.discountEditText);
        wholeDealDiscountAddButton = (Button) findViewById(R.id.addDiscountButton);
        finalTotalTextView = (TextView) findViewById(R.id.finalTotalValueText);
        sendDealMailButton = (Button) findViewById(R.id.sendDealMailButton);

        discountLayout = (LinearLayout) findViewById(R.id.discountLayout);

        Bundle extras = getIntent().getExtras();
        dealId = extras.getLong("DEAL_ID");
        routePath = extras.getInt("ROUTE");

        getDeal = localDatabaseSQLiteOpenHelper.getDealFromDealId(dealId);
        getCustomer = localDatabaseSQLiteOpenHelper.getCustomerFromCustomerId(getDeal.getCustomerId());
        productDealsList = localDatabaseSQLiteOpenHelper.getProductDealsFromDealId(dealId);

        dealNameText.setText("Deal Name : " + getDeal.getDealName());
        dealCustomerNameText.setText("Customer Name : " + getCustomer.getCustomerName());
        dealCustomerContactText.setText("Customer Contact No. :" + getCustomer.getCustomerContactNo());
        dealDateText.setText("Deal Date : " + getDeal.getDealDate());

        int i = 1;
        for (ProductDeal productDeal : productDealsList) {

            TableRow row= new TableRow(this);
            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row.setLayoutParams(lp);
            TextView name = new TextView(this);
            TextView uniPrise = new TextView(this);
            TextView qty = new TextView(this);
            TextView discount = new TextView(this);
            TextView total = new TextView(this);

            name.setText(productDeal.getProductName());
            name.setGravity(Gravity.CENTER);
            name.setTextSize(16);
            uniPrise.setText(String.valueOf(productDeal.getUnitPrice()));
            uniPrise.setGravity(Gravity.CENTER);
            uniPrise.setTextSize(16);
            qty.setText(String.valueOf(productDeal.getQuantity()));
            qty.setGravity(Gravity.CENTER);
            qty.setTextSize(16);
            discount.setText(String.valueOf(productDeal.getDiscount()));
            discount.setGravity(Gravity.CENTER);
            discount.setTextSize(16);
            total.setText(String.valueOf(productDeal.getTotalAmount()));
            total.setGravity(Gravity.CENTER);
            total.setTextSize(16);

            row.addView(name);
            row.addView(uniPrise);
            row.addView(qty);
            row.addView(discount);
            row.addView(total);

            tableView.addView(row,i);
            i++;
        }

        /* this following content is for showing total amount of the deal */
        TableRow row= new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);
        TextView name = new TextView(this);
        TextView uniPrise = new TextView(this);
        TextView qty = new TextView(this);
        TextView discount = new TextView(this);
        TextView total = new TextView(this);

        name.setText("");
        name.setGravity(Gravity.CENTER);
        name.setTextSize(16);
        uniPrise.setText("");
        uniPrise.setGravity(Gravity.CENTER);
        uniPrise.setTextSize(16);
        qty.setText("");
        qty.setGravity(Gravity.CENTER);
        qty.setTextSize(16);
        discount.setText("Total : ");
        discount.setGravity(Gravity.CENTER);
        discount.setTextSize(16);
        total.setText(String.valueOf(getDeal.getDealTotalAmount()));
        total.setGravity(Gravity.CENTER);
        total.setTextSize(16);

        row.addView(name);
        row.addView(uniPrise);
        row.addView(qty);
        row.addView(discount);
        row.addView(total);

        tableView.addView(row);
        /* this above content is for showing total amount of the deal */

        finalTotalTextView.setText("Total : " + getDeal.getDealTotalAmount() + " LKR");

        if(routePath != 1) {
            discountLayout.setEnabled(false);
            wholeDealDiscountEditText.setText(String.valueOf(getDeal.getDealDiscount()));
            wholeDealDiscountEditText.setEnabled(false);
            wholeDealDiscountAddButton.setEnabled(false);
        } else {

            wholeDealDiscountAddButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String getWholeDiscount = wholeDealDiscountEditText.getText().toString();
                    Double wholeDiscount = 0.0;
                    if(getWholeDiscount != null && !getWholeDiscount.equals("")) {
                        wholeDiscount = Double.parseDouble(getWholeDiscount);
                    }

                    Double currentTotal = getDeal.getDealTotalAmount();
                    Double newTotal = currentTotal * ((100 - wholeDiscount)/100);

                    localDatabaseSQLiteOpenHelper.updateDealTotalAndDiscountAmount(wholeDiscount, newTotal, dealId);

                    finalTotalTextView.setText("Total : " + newTotal + " LKR");
                }
            });
        }

        sendDealMailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                List<String> emails = localDatabaseSQLiteOpenHelper.getAllEmails();
                String[] emailsArray = new String[100];
                int index = 0;
                for (String email : emails) {
                    emailsArray[index] = email;
                    index++;
                }

                String subject = "Deal: " + getDeal.getDealName() + " - details summery - " + getDeal.getDealDate();
                String message = "Deal ID : " + getDeal.getDealId() + "\n" +
                        "Deal Name : " + getDeal.getDealName() + "\n" +
                        "Customer ID : " + getCustomer.getCustomerId() + "\n" +
                        "Customer Name : " + getCustomer.getCustomerName() + "\n" +
                        "Contact No. : " + getCustomer.getCustomerContactNo() + "\n" +
                        "Discount : " + getDeal.getDealDiscount() + "\n" +
                        "Final Amount : " + getDeal.getDealTotalAmount() + "\n" +
                        "Deal Date : " + getDeal.getDealDate() + "\n\n";

                String headings = String.format("%-20s %20s %20s %20s %20s",
                        "Product_Name",
                        "Unit_Price",
                        "Qty",
                        "Discount",
                        "Total_price");

                message = message + headings + "\n";

                for (ProductDeal getProductDeal : productDealsList) {

                    String contentRowString = String.format("%-20s %20s %20s %20s %20s",
                            getProductDeal.getProductName(),
                            getProductDeal.getUnitPrice(),
                            getProductDeal.getQuantity(),
                            getProductDeal.getDiscount(),
                            getProductDeal.getTotalAmount());

                    message = message + contentRowString + "\n";
                }

                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.putExtra(Intent.EXTRA_EMAIL, emailsArray);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
                emailIntent.putExtra(Intent.EXTRA_TEXT, message);

                emailIntent.setType("plain/text");

                startActivity(Intent.createChooser(emailIntent, "Choose and email client : "));

                finish();
            }
        });
    }
}
