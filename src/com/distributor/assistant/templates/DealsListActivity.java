package com.distributor.assistant.templates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.distributor.assistant.R;
import com.distributor.assistant.database.Deal;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kavi on 9/16/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class DealsListActivity extends Activity {

    private ListView dealsListView;
    private ArrayAdapter<String> dealsListAdapter;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deals_layout);

        setUpView();
    }

    private void setUpView() {
        dealsListView = (ListView) findViewById(R.id.dealsListView);

        List<String> dealNameList = new ArrayList<String>();
        List<Deal> dealList = localDatabaseSQLiteOpenHelper.getAllDeals();

        if(!dealList.isEmpty()) {
            for (Deal deal : dealList) {
                dealNameList.add(deal.getDealName() + " - " + deal.getDealId());
            }
        }

        dealsListAdapter = new ArrayAdapter<String>(this, R.layout.deal_row_view, dealNameList);
        dealsListView.setAdapter(dealsListAdapter);

        dealsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String itemContent = (String) (dealsListView.getItemAtPosition(i));


                String[] contents = itemContent.split("-");
                String id = contents[1].replace(" ", "");
                long idInLong = Long.parseLong(id);

                Intent dealSummeryIntent = new Intent(DealsListActivity.this, DealSummeryActivity.class);
                dealSummeryIntent.putExtra("DEAL_ID", idInLong);
                dealSummeryIntent.putExtra("ROUTE", 2);
                startActivity(dealSummeryIntent);
            }
        });
    }
}
