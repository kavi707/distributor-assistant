package com.distributor.assistant.templates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.distributor.assistant.R;
import com.distributor.assistant.database.Customer;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kavi on 9/16/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class SelectCustomerActivity extends Activity {

    private ListView customerListView;
    private ArrayAdapter<String> customerListAdapter;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_customer_layout);

        setUpView();
    }

    private void setUpView() {

        customerListView = (ListView) findViewById(R.id.customerListView);

        List<String> customerNameList = new ArrayList<String>();
        List<Customer> customerList = localDatabaseSQLiteOpenHelper.getAllCustomers();

        customerNameList.add("Add New Customer");

        if(!customerList.isEmpty()) {
            for (Customer customer : customerList) {
                customerNameList.add(customer.getCustomerName() + " - " + customer.getCustomerId());
            }
        }

        customerListAdapter = new ArrayAdapter<String>(this, R.layout.customer_row_view, customerNameList);
        customerListView.setAdapter(customerListAdapter);

        customerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String itemContent = (String) (customerListView.getItemAtPosition(i));
                if(itemContent.equals("Add New Customer")) {
                    Intent addCustomerIntent = new Intent(SelectCustomerActivity.this, AddCustomerActivity.class);
                    startActivity(addCustomerIntent);
                    finish();
                } else {
                    String[] contents = itemContent.split("-");
                    String id = contents[1].replace(" ", "");

                    Intent addDealIntent = new Intent(SelectCustomerActivity.this, AddDealActivity.class);
                    addDealIntent.putExtra("CUSTOMER_ID", id);
                    startActivity(addDealIntent);
                    finish();
                }
            }
        });
    }
}
