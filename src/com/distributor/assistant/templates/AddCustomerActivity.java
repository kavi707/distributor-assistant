package com.distributor.assistant.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.distributor.assistant.R;
import com.distributor.assistant.database.Customer;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by kavi on 9/17/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class AddCustomerActivity extends Activity {

    private EditText customerNameEditText;
    private EditText customerAddressEditText;
    private EditText contactNoEditText;

    private Button addCustomerButton;
    private Button resetButton;

    final Context context = this;
    private AlertDialog messageBalloonAlertDialog;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_customer);

        setUpView();
    }

    private void setUpView() {
        customerNameEditText = (EditText) findViewById(R.id.customerNameEditText);
        customerAddressEditText = (EditText) findViewById(R.id.customerAddressEditText);
        contactNoEditText = (EditText) findViewById(R.id.customerContactEditText);

        addCustomerButton = (Button) findViewById(R.id.addCustomerButton);
        resetButton = (Button) findViewById(R.id.resetAddCustomerButton);

        addCustomerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String customerName = customerNameEditText.getText().toString();
                String customerAddress = customerAddressEditText.getText().toString();
                String contactNo = contactNoEditText.getText().toString();

                if(customerName.equals(null) || customerName.equals("")){
                    messageBalloonAlertDialog = new AlertDialog.Builder(context)
                            .setTitle(R.string.warning)
                            .setMessage("Customer name is required")
                            .setNegativeButton(R.string.ok, new AlertDialog.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    messageBalloonAlertDialog.cancel();
                                }
                            }).create();
                    messageBalloonAlertDialog.show();
                } else {
                    Customer customer = new Customer();
                    customer.setCustomerName(customerName);
                    customer.setCustomerAddress(customerAddress);
                    customer.setCustomerContactNo(contactNo);

                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a");
                    customer.setAddedDate(sdf.format(date));
                    customer.setDateForNextVisit(sdf.format(date));

                    final long statusId = localDatabaseSQLiteOpenHelper.saveNewCustomer(customer);

                    if(statusId != 0) {
                        //TODO need to handle the user press back button
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.success)
                                .setMessage("New customer added successfully")
                                .setPositiveButton("Add the deal", new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent addNewDealIntent = new Intent(AddCustomerActivity.this, AddDealActivity.class);
                                        addNewDealIntent.putExtra("CUSTOMER_ID", statusId);
                                        startActivity(addNewDealIntent);
                                        finish();
                                    }
                                })
                                .setNeutralButton("Customer List", new AlertDialog.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Intent customerListIntent = new Intent(AddCustomerActivity.this, SelectCustomerActivity.class);
                                        startActivity(customerListIntent);
                                        finish();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    } else {
                        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                                .setTitle(R.string.error)
                                .setMessage("Error occurred while adding new customer. Please try again later")
                                .setNegativeButton(R.string.ok, new AlertDialog.OnClickListener(){
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        messageBalloonAlertDialog.cancel();
                                    }
                                }).create();
                        messageBalloonAlertDialog.show();
                    }
                }
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customerNameEditText.setText(null);
                customerAddressEditText.setText(null);
                contactNoEditText.setText(null);
            }
        });
    }
}
