package com.distributor.assistant.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.distributor.assistant.R;

/**
 * Created by kavi on 9/23/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class AddProductDealActivity extends Activity {

    private TextView dealIdTextView;
    private Spinner productNameSpinner;
    private EditText productWeightEditText;
    private EditText unitPriceEditText;
    private EditText qtyEditText;
    private EditText discountEditText;

    private Button checkDealButton;

    final Context context = this;
    private AlertDialog messageBalloonAlertDialog;

    private long dealId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_deal_layout);

        setUpView();
    }

    private void setUpView() {

        dealIdTextView = (TextView) findViewById(R.id.dealIdTextView);
        productNameSpinner = (Spinner) findViewById(R.id.productNameSpinner);
        productWeightEditText = (EditText) findViewById(R.id.productDealWeightEditText);
        unitPriceEditText = (EditText) findViewById(R.id.productUnitPriceEditText);
        qtyEditText = (EditText) findViewById(R.id.productQtyEditText);
        discountEditText = (EditText) findViewById(R.id.productDiscountEditText);

        checkDealButton = (Button) findViewById(R.id.checkDealButton);

        Bundle extras = getIntent().getExtras();
        String route = extras.getString("ROUTE");
        dealId = (extras.getLong("DEAL_ID"));
        dealIdTextView.setText("Deal ID : " + dealId);
        if(!route.equals(null) && route.equals("checkProductDealActivity")) {
            String productName = extras.getString("PRODUCT_NAME");
            String weight = extras.getString("PRODUCT_WEIGHT");
            String unitPrice = extras.getString("UNIT_PRICE");
            String qty = extras.getString("QTY");
            String discount = extras.getString("DISCOUNT");

            if(productName.equals("Cashew")) {
                productNameSpinner.setSelection(1);
            } else if(productName.equals("Deviled cashew")) {
                productNameSpinner.setSelection(2);
            } else if(productName.equals("Owen cashew")) {
                productNameSpinner.setSelection(3);
            } else if(productName.equals("Cocktail")) {
                productNameSpinner.setSelection(4);
            } else if(productName.equals("Others")) {
                productNameSpinner.setSelection(5);
            }

            productWeightEditText.setText(weight);
            unitPriceEditText.setText(unitPrice);
            qtyEditText.setText(qty);
            discountEditText.setText(discount);
        }

        checkDealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int nullStatus = 0;

                String productName = productNameSpinner.getSelectedItem().toString();
                String productWeight = productWeightEditText.getText().toString();
                String unitPrice = unitPriceEditText.getText().toString();
                String qty = qtyEditText.getText().toString();
                String discount = "";
                discount = discountEditText.getText().toString();

                if(productName == null || productName.equals("--- select product ---")) {
                    nullStatus = 1;
                } else if(productWeight == null || productWeight.equals("")) {
                    nullStatus = 2;
                } else if(unitPrice == null || unitPrice.equals("")) {
                    nullStatus = 3;
                } else if(qty == null || qty.equals("")) {
                    nullStatus = 4;
                } else if(discount == null || discount.equals("")) {
                    nullStatus = 5;
                }

                Double totalPrice;
                Double unitPriceValue;
                Double qtyValue;
                Double discountValue;


                Intent checkProductDealIntent = new Intent(AddProductDealActivity.this, CheckProductDealActivity.class);
                checkProductDealIntent.putExtra("DEAL_ID", dealId);
                checkProductDealIntent.putExtra("PRODUCT_NAME", productName);
                checkProductDealIntent.putExtra("PRODUCT_WEIGHT", productWeight);
                checkProductDealIntent.putExtra("UNIT_PRICE", unitPrice);
                checkProductDealIntent.putExtra("QTY", qty);

                switch (nullStatus) {
                    case 0:
                        unitPriceValue = Double.parseDouble(unitPrice);
                        qtyValue = Double.parseDouble(qty);
                        discountValue = Double.parseDouble(discount);
                        totalPrice = unitPriceValue * qtyValue * ((100 - discountValue)/100) ;

                        checkProductDealIntent.putExtra("DISCOUNT", discount);
                        checkProductDealIntent.putExtra("TOTAL", totalPrice);
                        startActivity(checkProductDealIntent);
                        finish();
                        break;
                    case 1:
                        showMessageBalloons("Product name is not selected. Please select a product name");
                        break;
                    case 2:
                        showMessageBalloons("Please give a product weight, for calculation");
                        break;
                    case 3:
                        showMessageBalloons("Please give a unit price, for calculation");
                        break;
                    case 4:
                        showMessageBalloons("Please give a quantity, for calculation");
                        break;
                    case 5:
                        unitPriceValue = Double.parseDouble(unitPrice);
                        qtyValue = Double.parseDouble(qty);
                        totalPrice = unitPriceValue * qtyValue;

                        checkProductDealIntent.putExtra("DISCOUNT", discount);
                        checkProductDealIntent.putExtra("TOTAL", totalPrice);
                        startActivity(checkProductDealIntent);
                        finish();
                        break;
                }
            }
        });
    }

    private void showMessageBalloons (String msg) {
        messageBalloonAlertDialog = new AlertDialog.Builder(context)
                .setTitle(R.string.warning)
                .setMessage(msg)
                .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        messageBalloonAlertDialog.cancel();
                    }
                }).create();
        messageBalloonAlertDialog.show();
    }
}
