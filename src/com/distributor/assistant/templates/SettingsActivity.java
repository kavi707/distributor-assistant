package com.distributor.assistant.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.distributor.assistant.R;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kavi on 10/4/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class SettingsActivity extends Activity {

    private EditText newEmailEditText;
    private Button newEmailAddButton;
    private ListView emailsListView;

    final Context context = this;
    private AlertDialog messageBalloonAlertDialog;

    private ArrayAdapter<String> emailsListAdapter;
    List<String> emailsList;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        setUpView();
    }

    private void setUpView() {

        newEmailEditText = (EditText) findViewById(R.id.newEmailEditText);
        newEmailAddButton = (Button) findViewById(R.id.addNewEmailButton);
        emailsListView = (ListView) findViewById(R.id.emailsListView);

        newEmailAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = newEmailEditText.getText().toString();
                newEmailEditText.setText(null);
                localDatabaseSQLiteOpenHelper.saveEmail(email);

                emailsList = localDatabaseSQLiteOpenHelper.getAllEmails();

                if(!emailsList.isEmpty()) {
                    emailsListAdapter = new ArrayAdapter<String>(SettingsActivity.this, R.layout.customer_row_view, emailsList);
                    emailsListView.setAdapter(emailsListAdapter);
                }
            }
        });

        emailsList = localDatabaseSQLiteOpenHelper.getAllEmails();

        if(!emailsList.isEmpty()) {
            emailsListAdapter = new ArrayAdapter<String>(this, R.layout.customer_row_view, emailsList);
            emailsListView.setAdapter(emailsListAdapter);
        }

        emailsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                final String itemContent = (String) (emailsListView.getItemAtPosition(i));

                String message = "Email : " + itemContent + " is selected. What do you need to do?";

                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                        .setTitle("Selected Email")
                        .setMessage(message)
                        .setPositiveButton("Delete", new AlertDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                localDatabaseSQLiteOpenHelper.deleteGivenEmail(itemContent);

                                emailsList = localDatabaseSQLiteOpenHelper.getAllEmails();

                                if(!emailsList.isEmpty()) {
                                    emailsListAdapter = new ArrayAdapter<String>(SettingsActivity.this, R.layout.customer_row_view, emailsList);
                                    emailsListView.setAdapter(emailsListAdapter);
                                }
                            }
                        })
                        .setNeutralButton(R.string.cancel, new AlertDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                messageBalloonAlertDialog.cancel();
                            }
                        }).create();
                messageBalloonAlertDialog.show();
            }
        });
    }
}
