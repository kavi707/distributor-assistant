package com.distributor.assistant.templates;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.distributor.assistant.R;
import com.distributor.assistant.database.Customer;
import com.distributor.assistant.database.LocalDatabaseSQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kavi on 10/2/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class CustomerListActivity extends Activity {

    private ListView customerListView;
    private ArrayAdapter<String> customerListAdapter;

    final Context context = this;
    private AlertDialog messageBalloonAlertDialog;

    private LocalDatabaseSQLiteOpenHelper localDatabaseSQLiteOpenHelper = new LocalDatabaseSQLiteOpenHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_list_layout);

        setUpViews();
    }

    private void setUpViews() {

        customerListView = (ListView) findViewById(R.id.showCustomerListView);

        List<String> customerNameList = new ArrayList<String>();
        List<Customer> customerList = localDatabaseSQLiteOpenHelper.getAllCustomers();

        if(!customerList.isEmpty()) {
            for (Customer customer : customerList) {
                customerNameList.add(customer.getCustomerName() + " - " + customer.getCustomerId());
            }
        }

        customerListAdapter = new ArrayAdapter<String>(this, R.layout.customer_row_view, customerNameList);
        customerListView.setAdapter(customerListAdapter);

        customerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String itemContent = (String) (customerListView.getItemAtPosition(i));

                String[] contents = itemContent.split("-");
                String id = contents[1].replace(" ", "");

                Customer getCustomer = localDatabaseSQLiteOpenHelper.getCustomerFromCustomerId(Integer.parseInt(id));

                String message = "Customer name : " + getCustomer.getCustomerName() + "\n\n"
                        + "Customer address : " + getCustomer.getCustomerAddress() + "\n\n"
                        + "Contact No. : " + getCustomer.getCustomerContactNo();
                messageBalloonAlertDialog = new AlertDialog.Builder(context)
                        .setTitle("Selected Customer Details")
                        .setMessage(message)
                        .setNeutralButton(R.string.ok, new AlertDialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                messageBalloonAlertDialog.cancel();
                            }
                        }).create();
                messageBalloonAlertDialog.show();
            }
        });
    }
}
