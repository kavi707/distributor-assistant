package com.distributor.assistant.templates;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.distributor.assistant.R;

/**
 * Created by kavi on 9/15/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class MenuActivity extends Activity {

    private Button addDealButton;
    private Button checkDealsButton;
    private Button customersButton;
    private Button settingsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_layout);

        setUpView();
    }

    private void setUpView() {

        addDealButton = (Button) findViewById(R.id.addDealButton);
        checkDealsButton = (Button) findViewById(R.id.checkDealsButton);
        customersButton = (Button) findViewById(R.id.customersButton);
        settingsButton = (Button) findViewById(R.id.settingsButton);

        addDealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent customerListIntent = new Intent(MenuActivity.this, SelectCustomerActivity.class);
                startActivity(customerListIntent);
            }
        });

        checkDealsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dealsListIntent = new Intent(MenuActivity.this, DealsListActivity.class);
                startActivity(dealsListIntent);
            }
        });

        customersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent customersIntent = new Intent(MenuActivity.this, CustomerListActivity.class);
                startActivity(customersIntent);
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settingsIntent = new Intent(MenuActivity.this, SettingsActivity.class);
                startActivity(settingsIntent);
            }
        });
    }
}
