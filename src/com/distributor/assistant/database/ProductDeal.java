package com.distributor.assistant.database;

/**
 * Created by kavi on 9/17/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class ProductDeal {

    private int productDealId;
    private long dealId;
    private String productName;
    private Double weight;
    private Double unitPrice;
    private Double quantity;
    private Double discount;
    private Double totalAmount;


    public int getProductDealId() {
        return productDealId;
    }

    public void setProductDealId(int productDealId) {
        this.productDealId = productDealId;
    }

    public long getDealId() {
        return dealId;
    }

    public void setDealId(long dealId) {
        this.dealId = dealId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }
}
