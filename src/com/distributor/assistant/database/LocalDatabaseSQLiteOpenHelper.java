package com.distributor.assistant.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kavi on 9/17/13.
 * @author Kavimal Wijewardana <kavi707@gmail.com>
 */
public class LocalDatabaseSQLiteOpenHelper extends SQLiteOpenHelper {

    private SQLiteDatabase localDistributorDatabase;

    public static final String DB_NAME = "local_distributor_db.sqlite";
    public static final int VERSION = 1;

    //customer table columns
    public static final String CUSTOMER_TABLE_NAME = "customer";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String CUSTOMER_NAME = "customer_name";
    public static final String CUSTOMER_ADDRESS = "address";
    public static final String CUSTOMER_CONTACT_NO = "contact_no";
    public static final String CUSTOMER_ADDED_DATE = "customer_added_date";
    public static final String NEXT_VISIT_DATE = "next_visit_date";

    //product deals table columns
    public static final String PRODUCT_DEALS_TABLE_NAME = "product_deals";
    public static final String PRODUCT_DEAL_ID = "product_deal_id";
    public static final String PRODUCT_NAME = "product_name";
    public static final String WEIGHT = "weight";
    public static final String UNIT_PRICE = "unit_price";
    public static final String QTY = "qty";
    public static final String DISCOUNT = "discount";
    public static final String TOTAL_AMOUNT = "total_amount";

    //deals table columns
    public static final String DEALS_TABLE_NAME = "deals";
    public static final String DEAL_ID = "deal_id";
    public static final String DEAL_NAME = "deal_name";
    public static final String DEAL_DATE = "deal_date";
    public static final String DEAL_DISCOUNT = "deal_discount";
    public static final String DEAL_TOTAL_AMOUNT = "deal_total";

    //emails table columns
    public static final String EMAILS_TABLE_NAME = "emails";
    public static final String EMAIL_ID = "email_id";
    public static final String EMAIL = "email";

    public LocalDatabaseSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createCustomerTable(sqLiteDatabase);
        createProductDealsTable(sqLiteDatabase);
        createDealsTable(sqLiteDatabase);
        createEmailsTable(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }

    private void createCustomerTable(SQLiteDatabase sqLiteDatabase) {
        String createCustomerTableQuery = "create table " + CUSTOMER_TABLE_NAME + " ( " +
                CUSTOMER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                CUSTOMER_NAME + " text, " +
                CUSTOMER_ADDRESS + " text, " +
                CUSTOMER_CONTACT_NO + " text, " +
                CUSTOMER_ADDED_DATE + " date, " +
                NEXT_VISIT_DATE + " date " +
                ");";
        sqLiteDatabase.execSQL(createCustomerTableQuery);
    }

    private void createProductDealsTable(SQLiteDatabase sqLiteDatabase) {
        String createProductDealsTableQuery = "create table " + PRODUCT_DEALS_TABLE_NAME + " ( " +
                PRODUCT_DEAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                DEAL_ID + " int, " +
                PRODUCT_NAME + " text, " +
                WEIGHT + " float, " +
                UNIT_PRICE + " float, " +
                QTY + " float, " +
                DISCOUNT + " float, " +
                TOTAL_AMOUNT + " float " +
                ");";
        sqLiteDatabase.execSQL(createProductDealsTableQuery);
    }

    private void createDealsTable(SQLiteDatabase sqLiteDatabase) {
        String createDealsTableQuery = "create table " + DEALS_TABLE_NAME + " ( " +
                DEAL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                DEAL_NAME + " text, " +
                CUSTOMER_ID + " int, " +
                DEAL_DATE + " text, " +
                DEAL_DISCOUNT + " float, " +
                DEAL_TOTAL_AMOUNT + " float " +
                ");";
        sqLiteDatabase.execSQL(createDealsTableQuery);
    }

    private void createEmailsTable(SQLiteDatabase sqLiteDatabase) {
        String createEmailsTableQuery = "create table " + EMAILS_TABLE_NAME + " ( " +
                EMAIL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT not null, " +
                EMAIL + " text " +
                ");";
        sqLiteDatabase.execSQL(createEmailsTableQuery);
    }


    public long saveNewCustomer(Customer customer) {

        long statusId = 0;

        localDistributorDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(CUSTOMER_NAME, customer.getCustomerName());
        values.put(CUSTOMER_ADDRESS, customer.getCustomerAddress());
        values.put(CUSTOMER_CONTACT_NO, customer.getCustomerContactNo());
        values.put(CUSTOMER_ADDED_DATE, customer.getAddedDate());
        values.put(NEXT_VISIT_DATE, customer.getDateForNextVisit());

        try {
            statusId = localDistributorDatabase.insert(CUSTOMER_TABLE_NAME, null, values);
        } catch (SQLiteException ex) {
            statusId = 0;
            throw ex;
        }

        return statusId;
    }

    public List<Customer> getAllCustomers() {
        localDistributorDatabase = this.getWritableDatabase();
        List<Customer> customerList = new ArrayList<Customer>();

        try {
            String getAllCustomersQry = "SELECT * FROM " + CUSTOMER_TABLE_NAME;
            Cursor customerCursor = localDistributorDatabase.rawQuery(getAllCustomersQry, null);
            customerCursor.moveToFirst();

            if(!customerCursor.isAfterLast()) {
                do {
                    int customerId = customerCursor.getInt(0);
                    String customerName = customerCursor.getString(1);
                    String customerAddress = customerCursor.getString(2);
                    String contactNo = customerCursor.getString(3);
                    String addedDate = customerCursor.getString(4);
                    String nextDate = customerCursor.getString(5);

                    Customer customer = new Customer();
                    customer.setCustomerId(customerId);
                    customer.setCustomerName(customerName);
                    customer.setCustomerAddress(customerAddress);
                    customer.setCustomerContactNo(contactNo);
                    customer.setAddedDate(addedDate);
                    customer.setDateForNextVisit(nextDate);

                    customerList.add(customer);
                }while (customerCursor.moveToNext());
            }
            customerCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return customerList;
    }

    public Customer getCustomerFromCustomerId (int customerId) {
        localDistributorDatabase = this.getWritableDatabase();
        Customer getCustomer = new Customer();
        List<Customer> tempCustomerList = new ArrayList<Customer>();

        try {
            String getCustomersQry = "SELECT * FROM " + CUSTOMER_TABLE_NAME + " WHERE " + CUSTOMER_ID + "=" + customerId;
            Cursor customerCursor = localDistributorDatabase.rawQuery(getCustomersQry, null);
            customerCursor.moveToFirst();

            if(!customerCursor.isAfterLast()) {
                do {
                    int getCustomerId = customerCursor.getInt(0);
                    String customerName = customerCursor.getString(1);
                    String customerAddress = customerCursor.getString(2);
                    String contactNo = customerCursor.getString(3);
                    String addedDate = customerCursor.getString(4);
                    String nextDate = customerCursor.getString(5);

                    Customer customer = new Customer();
                    customer.setCustomerId(getCustomerId);
                    customer.setCustomerName(customerName);
                    customer.setCustomerAddress(customerAddress);
                    customer.setCustomerContactNo(contactNo);
                    customer.setAddedDate(addedDate);
                    customer.setDateForNextVisit(nextDate);

                    tempCustomerList.add(customer);
                }while (customerCursor.moveToNext());
            }
            customerCursor.close();

            if(!tempCustomerList.isEmpty()) {
                getCustomer = tempCustomerList.get(0);
            }
        } catch (SQLiteException ex) {
            throw ex;
        }

        return getCustomer;
    }

    public long saveDeal(Deal deal) {

        long statusId = 0;

        localDistributorDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DEAL_NAME, deal.getDealName());
        values.put(CUSTOMER_ID, deal.getCustomerId());
        values.put(DEAL_DATE, deal.getDealDate());
        values.put(DEAL_DISCOUNT, deal.getDealDiscount());
        values.put(DEAL_TOTAL_AMOUNT, deal.getDealTotalAmount());

        try {
            statusId = localDistributorDatabase.insert(DEALS_TABLE_NAME, null, values);
        } catch (SQLiteException ex) {
            statusId = 0;
            throw ex;
        }

        return statusId;
    }

    public void updateDealTotalAndDiscountAmount(Double wholeDiscount, Double newTotal, long dealId) {

        localDistributorDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DEAL_DISCOUNT, wholeDiscount);
        values.put(DEAL_TOTAL_AMOUNT, newTotal);
        String where = DEAL_ID + "=" + dealId;
        try {
            localDistributorDatabase.update(DEALS_TABLE_NAME, values, where, null);
        } catch (SQLiteException ex) {
            throw ex;
        }
    }

    public List<Deal> getAllDeals() {
        localDistributorDatabase = this.getWritableDatabase();
        List<Deal> deals = new ArrayList<Deal>();

        try {
            String getAllDealsQry = "SELECT * FROM " + DEALS_TABLE_NAME;
            Cursor dealCursor = localDistributorDatabase.rawQuery(getAllDealsQry, null);
            dealCursor.moveToFirst();

            if(!dealCursor.isAfterLast()) {
                do {
                    int dealId = dealCursor.getInt(0);
                    String dealName = dealCursor.getString(1);
                    int customerId = dealCursor.getInt(2);
                    String dealDate = dealCursor.getString(3);
                    Double dealDiscount = dealCursor.getDouble(4);
                    Double dealTotal = dealCursor.getDouble(5);

                    Deal deal = new Deal();
                    deal.setDealId(dealId);
                    deal.setDealName(dealName);
                    deal.setCustomerId(customerId);
                    deal.setDealDate(dealDate);
                    deal.setDealDiscount(dealDiscount);
                    deal.setDealTotalAmount(dealTotal);

                    deals.add(deal);
                } while (dealCursor.moveToNext());
            }
            dealCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return deals;
    }

    public Deal getDealFromDealId (long dealId) {
        localDistributorDatabase = this.getWritableDatabase();
        Deal getDeal = new Deal();
        List<Deal> tempDealList = new ArrayList<Deal>();

        try {
            String getDealFromDealIdQry = "SELECT * FROM " + DEALS_TABLE_NAME + " WHERE " + DEAL_ID + "=" + dealId;
            Cursor dealCursor = localDistributorDatabase.rawQuery(getDealFromDealIdQry, null);

            dealCursor.moveToFirst();

            if(!dealCursor.isAfterLast()) {
                do {
                    int getDealId = dealCursor.getInt(0);
                    String dealName = dealCursor.getString(1);
                    int customerId = dealCursor.getInt(2);
                    String dealDate = dealCursor.getString(3);
                    Double dealDiscount = dealCursor.getDouble(4);
                    Double dealTotal = dealCursor.getDouble(5);

                    Deal deal = new Deal();
                    deal.setDealId(getDealId);
                    deal.setDealName(dealName);
                    deal.setCustomerId(customerId);
                    deal.setDealDate(dealDate);
                    deal.setDealDiscount(dealDiscount);
                    deal.setDealTotalAmount(dealTotal);

                    tempDealList.add(deal);
                } while (dealCursor.moveToNext());
            }
            dealCursor.close();
            if(!tempDealList.isEmpty()) {
                getDeal = tempDealList.get(0);
            }
        } catch (SQLiteException ex) {
            throw ex;
        }
        return getDeal;
    }

    public void saveProductDeal(ProductDeal productDeal) {
        localDistributorDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(DEAL_ID, productDeal.getDealId());
        values.put(PRODUCT_NAME, productDeal.getProductName());
        values.put(WEIGHT, productDeal.getWeight());
        values.put(UNIT_PRICE, productDeal.getUnitPrice());
        values.put(QTY, productDeal.getQuantity());
        values.put(DISCOUNT, productDeal.getDiscount());
        values.put(TOTAL_AMOUNT, productDeal.getTotalAmount());

        try {
            localDistributorDatabase.insert(PRODUCT_DEALS_TABLE_NAME, null, values);
        } catch (SQLiteException ex) {
            throw ex;
        }
    }

    public List<ProductDeal> getAllProductDeals() {
        localDistributorDatabase = this.getWritableDatabase();
        List<ProductDeal> productDealsList = new ArrayList<ProductDeal>();

        try {
            String getAllProductDealsQry = "SELECT * FROM " + PRODUCT_DEALS_TABLE_NAME;
            Cursor productDealCursor = localDistributorDatabase.rawQuery(getAllProductDealsQry, null);
            productDealCursor.moveToFirst();

            if(!productDealCursor.isAfterLast()) {
                do {
                    int productDealId = productDealCursor.getInt(0);
                    long dealId = productDealCursor.getLong(1);
                    String productName = productDealCursor.getString(2);
                    Double weight = productDealCursor.getDouble(3);
                    Double unitPrice = productDealCursor.getDouble(4);
                    Double qty = productDealCursor.getDouble(5);
                    Double discount = productDealCursor.getDouble(6);
                    Double totalAmount = productDealCursor.getDouble(7);

                    ProductDeal productDeal = new ProductDeal();
                    productDeal.setProductDealId(productDealId);
                    productDeal.setDealId(dealId);
                    productDeal.setProductName(productName);
                    productDeal.setWeight(weight);
                    productDeal.setUnitPrice(unitPrice);
                    productDeal.setQuantity(qty);
                    productDeal.setDiscount(discount);
                    productDeal.setTotalAmount(totalAmount);

                    productDealsList.add(productDeal);
                } while (productDealCursor.moveToNext());
            }
            productDealCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return productDealsList;
    }

    public List<ProductDeal> getProductDealsFromDealId(long dealId) {

        localDistributorDatabase = this.getWritableDatabase();
        List<ProductDeal> productDealsList = new ArrayList<ProductDeal>();
        try {
            String productDealsFromDealIdQry = "SELECT * FROM " + PRODUCT_DEALS_TABLE_NAME + " WHERE " + DEAL_ID + "=" + dealId;
            Cursor productDealCursor = localDistributorDatabase.rawQuery(productDealsFromDealIdQry, null);

            productDealCursor.moveToFirst();

            if(!productDealCursor.isAfterLast()) {
                do {
                    int productDealId = productDealCursor.getInt(0);
                    long getDealId = productDealCursor.getLong(1);
                    String productName = productDealCursor.getString(2);
                    Double weight = productDealCursor.getDouble(3);
                    Double unitPrice = productDealCursor.getDouble(4);
                    Double qty = productDealCursor.getDouble(5);
                    Double discount = productDealCursor.getDouble(6);
                    Double totalAmount = productDealCursor.getDouble(7);

                    ProductDeal productDeal = new ProductDeal();
                    productDeal.setProductDealId(productDealId);
                    productDeal.setDealId(getDealId);
                    productDeal.setProductName(productName);
                    productDeal.setWeight(weight);
                    productDeal.setUnitPrice(unitPrice);
                    productDeal.setQuantity(qty);
                    productDeal.setDiscount(discount);
                    productDeal.setTotalAmount(totalAmount);

                    productDealsList.add(productDeal);
                } while (productDealCursor.moveToNext());
            }

            productDealCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return  productDealsList;
    }

    public void saveEmail(String email) {

        localDistributorDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(EMAIL, email);

        try {
            localDistributorDatabase.insert(EMAILS_TABLE_NAME, null, values);
        } catch (SQLiteException ex) {
            throw ex;
        }
    }

    public List<String> getAllEmails() {

        List<String> emails = new ArrayList<String>();
        localDistributorDatabase = this.getWritableDatabase();

        try {
            String getAllEmailsQry = "SELECT * FROM " + EMAILS_TABLE_NAME;
            Cursor emailsCursor = localDistributorDatabase.rawQuery(getAllEmailsQry, null);

            emailsCursor.moveToFirst();

            if(!emailsCursor.isAfterLast()) {
                do {
                    String email = emailsCursor.getString(1);
                    emails.add(email);
                }while (emailsCursor.moveToNext());
            }
            emailsCursor.close();
        } catch (SQLiteException ex) {
            throw ex;
        }

        return emails;
    }

    public void deleteGivenEmail(String email) {
        localDistributorDatabase = this.getWritableDatabase();

        String where = EMAIL + " = '" + email + "'";
        try {
            localDistributorDatabase.delete(EMAILS_TABLE_NAME, where, null);
        } catch (SQLiteException ex) {
            throw ex;
        }
    }
}
